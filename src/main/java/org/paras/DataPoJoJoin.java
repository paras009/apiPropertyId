package org.paras;

public class DataPoJoJoin {
	
	private int PMTRFNUM;
	private String MODIDATE;
	private String CATISVERIFIED;
	private String CATVERIFIEDDATE;
	private String CATVERIFICATIONCOMMENTS;
	
	
	public DataPoJoJoin(int pMTRFNUM, String mODIDATE, String cATISVERIFIED, String cATVERIFIEDDATE,
			String cATVERIFICATIONCOMMENTS) {
		super();
		PMTRFNUM = pMTRFNUM;
		MODIDATE = mODIDATE;
		CATISVERIFIED = cATISVERIFIED;
		CATVERIFIEDDATE = cATVERIFIEDDATE;
		CATVERIFICATIONCOMMENTS = cATVERIFICATIONCOMMENTS;
	}


	public int getPMTRFNUM() {
		return PMTRFNUM;
	}


	public String getMODIDATE() {
		return MODIDATE;
	}


	public String getCATISVERIFIED() {
		return CATISVERIFIED;
	}


	public String getCATVERIFIEDDATE() {
		return CATVERIFIEDDATE;
	}


	public String getCATVERIFICATIONCOMMENTS() {
		return CATVERIFICATIONCOMMENTS;
	}
	
	

}
