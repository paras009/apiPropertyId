package org.paras;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;

@Entity(name="tppmt")
public class DataPoJo {

	@Id
	private int PMTRFNUM;
	private String MODIDATE;


	@OneToOne(cascade = CascadeType.ALL)
	@JoinTable(name="tpcat", joinColumns = @JoinColumn(name="CATPMTRFNUM"),
	inverseJoinColumns = @JoinColumn(name="PMTRFNUM"))
	private DataPoJo2 pojo2;
	
	
	public DataPoJo2 getPojo2() {
		return pojo2;	
	}
	
	public DataPoJo(int pMTRFNUM, String mODIDATE, DataPoJo2 pojo2) {
		super();
		PMTRFNUM = pMTRFNUM;
		MODIDATE = mODIDATE;
		this.pojo2 = pojo2;
	}
	
	public DataPoJo() {}


	public void setPojo2(DataPoJo2 pojo2) {
		this.pojo2 = pojo2;
	}

	public int getPMTRFNUM() {
		return PMTRFNUM;
	}


	public String getMODIDATE() {
		return MODIDATE;
	}




	
	

}
