package org.paras.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.service.ServiceRegistry;
import org.paras.DataPoJo;
import org.paras.DataPoJo2;
import org.paras.DataPoJoJoin;

public class LoginService {
	
	/*String url = "jdbc:mysql://localhost:3306/CUSTOMERS";
	String sqlName = "root";
	String sqlPass = "qwe123qwe123";
	String sql = "select * from mb where id=?";
	

	String mbUrl = "jdbc:mysql://10.150.200.203:3310/property?zeroDateTimeBehavior=convertToNull";
	String mbUserName = "apptesting_user";
	String mbPass = "App123Test@r321";
	String mbSql = "select pmt.PMTRFNUM, pmt.MODIDATE, cat.CATISVERIFIED, cat.CATVERIFIEDDATE, cat.CATVERIFICATIONCOMMENTS from tppmt pmt join tpcat cat on pmt.PMTRFNUM = cat.CATPMTRFNUM where pmt.PMTRFNUM = ?";
	
*/
	
	public List<DataPoJoJoin> getAllData(String[] propIdArr) {
		
		
		List<DataPoJoJoin> myList = new ArrayList<DataPoJoJoin>();	
		
		DataPoJo pojo = new DataPoJo();
		DataPoJo2 pojo2 = new DataPoJo2();
		pojo.setPojo2(pojo2);
		
		
	
		
///////////////////////////////////////////////////////////////////////////////////////////////////
		
		/*for(int i=0; i<propIdArr.length; i++) {
			
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = (Connection) DriverManager.getConnection(url, sqlName, sqlPass);
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, propIdArr[i]);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				int id = Integer.parseInt(rs.getString("pmt.PMTRFNUM"));
				String date = rs.getString("pmt.MODIDATE");
				String isVarified = rs.getString("cat.CATISVERIFIED");
				String varifiedDate = rs.getString("cat.CATVERIFIEDDATE");
				String comment = rs.getString("cat.CATVERIFICATIONCOMMENTS");
				
				// with the mb database uncomment above code and comment this one and change url, username, sql and password.
				int id = Integer.parseInt(rs.getString("id"));
				String date = rs.getString("date");
				String isVarified = rs.getString("isVarified");
				String varifiedDate = rs.getString("varifiedDate");
				String comment = rs.getString("comment");
				
				
				myList.add(new DataPoJo(id, date, isVarified, varifiedDate, comment));
			}
			
			}catch(Exception e) {}
				
		}*/
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		Session session = null;
		Transaction tx = null;
		
			try {			
			Configuration config  = new Configuration().configure().addAnnotatedClass(DataPoJo.class).addAnnotatedClass(DataPoJo2.class);
			ServiceRegistry reg = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
			SessionFactory factory = config.buildSessionFactory(reg);
			session = factory.openSession();
			tx = session.beginTransaction();
			}catch(Exception ex) {
				System.out.println("PARAS CONNECTION ERROR CAME: "+ex.getMessage());
			}
					
			for(int i=0; i<propIdArr.length; i++) {
				
				
				try {
					DataPoJo current = session.get(DataPoJo.class, Integer.parseInt(propIdArr[i]));
					DataPoJo2 current2 = current.getPojo2();
					myList.add(new DataPoJoJoin(current.getPMTRFNUM(), current.getMODIDATE(), current2.getCATISVERIFIED(), current2.getCATVERIFIEDDATE(), current2.getCATVERIFICATIONCOMMENTS()));
					System.out.println(session.get(DataPoJo.class, Integer.parseInt(propIdArr[i])).getPMTRFNUM());
				}catch(Exception e) {
				System.out.println("PARAS ERROR CAME: "+e.getMessage());
				}
			}
				
			tx.commit();
			
		///////////////////////////////////////////////////////////////////////////////////////////////////

		return myList;
	}
}


























