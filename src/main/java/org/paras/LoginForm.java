package org.paras;

import java.util.ArrayList;
import java.util.List;

import org.paras.service.LoginService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginForm {
	
	@RequestMapping("/login")
	public ModelAndView loginUser(@RequestParam("propId") String propId) {
	System.out.println("user login do here in");
	
	String[] propIdArr = propId.split(",");
	
		
	List<DataPoJoJoin> myListData = new ArrayList<DataPoJoJoin>();
	
	LoginService loginService = new LoginService();
	ModelAndView mv = new ModelAndView();

		
			myListData = loginService.getAllData(propIdArr);
			
			
			StringBuffer proId = new StringBuffer();
			StringBuffer modDate = new StringBuffer();
			StringBuffer isVar = new StringBuffer();
			StringBuffer varDate = new StringBuffer();
			StringBuffer comm = new StringBuffer();

		
			for(int k=0; k<myListData.size(); k++) {
				
				proId.append(myListData.get(k).getPMTRFNUM()+",");
				modDate.append( myListData.get(k).getMODIDATE()+",");
				isVar.append( myListData.get(k).getCATISVERIFIED()+",");
				varDate.append(myListData.get(k).getCATVERIFIEDDATE()+",");
				comm.append( myListData.get(k).getCATVERIFICATIONCOMMENTS()+",");
			}
			
			
			mv.setViewName("loginUser.jsp");
			mv.addObject("propId", proId);
			mv.addObject("date", modDate);
			mv.addObject("isVarified", isVar);
			mv.addObject("varifiedDate",varDate);
			mv.addObject("comment", comm);
	
		return mv;
	}
	

}
